import { Controller, Get, Post, Body, Param, Put, Delete } from '@nestjs/common';
import { PrismaClient } from '@prisma/client';

const prisma = new PrismaClient();

@Controller('posts')
export class PostsController {
  @Get()
  async findAll() {
    return await prisma.post.findMany();
  }

  @Get(':id')
  async findOne(@Param('id') id: string) {
    return await prisma.post.findUnique({
      where: { id: Number(id) },
    });
  }

  @Post()
  async create(@Body() data: any) {
    return await prisma.post.create({
      data,
    });
  }

  @Put(':id')
  async update(@Param('id') id: string, @Body() data: any) {
    return await prisma.post.update({
      where: { id: Number(id) },
      data,
    });
  }

  @Delete(':id')
  async delete(@Param('id') id: string) {
    return await prisma.post.delete({
      where: { id: Number(id) },
    });
  }
}
