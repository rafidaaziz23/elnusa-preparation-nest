import { Controller, Get, Post, Body, Param, Put, Delete } from '@nestjs/common';
import { PrismaClient } from '@prisma/client';

const prisma = new PrismaClient();

@Controller('users')
export class UsersController {
  @Get()
  async findAll() {
    return await prisma.user.findMany();
  }

  @Get(':id')
  async findOne(@Param('id') id: string) {
    return await prisma.user.findUnique({
      where: { id: Number(id) },
    });
  }

  @Post()
  async create(@Body() data: any) {
    return await prisma.user.create({
      data,
    });
  }

  @Put(':id')
  async update(@Param('id') id: string, @Body() data: any) {
    return await prisma.user.update({
      where: { id: Number(id) },
      data,
    });
  }

  @Delete(':id')
  async delete(@Param('id') id: string) {
    return await prisma.user.delete({
      where: { id: Number(id) },
    });
  }
}
