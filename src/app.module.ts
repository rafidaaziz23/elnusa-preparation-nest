import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UsersModule } from './users/users.module';
import { PostsModule } from './posts/posts.module';
import { UsersController } from './users/users.controller';
import { PostsController } from './posts/posts.controller';

@Module({
  imports: [UsersModule, PostsModule],
  controllers: [AppController,UsersController, PostsController],
  providers: [AppService],
})
export class AppModule {}
